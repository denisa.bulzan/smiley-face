var e1, e2, e3;

function setup() {
  createCanvas(2048, 1026);
  noStroke();
  e1 = new Eye(140, 100, 40);
  e2 = new Eye(200, 100, 40);
}

function draw() {
  background(155);
  strokeWeight(0.3); 
  stroke(85);
  ellipse(170, 120, 150, 150);
  fill(255, 238, 29);
  e1.update(mouseX, mouseY);
  e2.update(mouseX, mouseY);
  e1.display();
  e2.display();
  strokeWeight(4); 
  stroke(229, 57, 12);
  curveDetail(50);
  curve(300, 10, 200, 150, 140, 150, 200, 140);


}

function Eye(tx, ty, ts) {
  this.x = tx;
  this.y = ty;
  this.size = ts;
  this.angle = 0;

  this.update = function (mx, my) {
    this.angle = atan2(my - this.y, mx - this.x);
  };

  this.display = function () {
    push();
    translate(this.x, this.y);
    fill(255);
    ellipse(0, 0, this.size, this.size);
    rotate(this.angle);
    fill(65);
    ellipse(this.size / 3, 0, this.size / 3, this.size / 3);
    pop();
  };
}